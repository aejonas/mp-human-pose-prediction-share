## Machine Perception - Human Motion Prediction

# Authors

Jonas Purtschert <jonaspu@ethz.ch>
Jonas Aeschbacher <jonas.aeschbacher@hotmail.com>

# Run The Code
## training
1. Clone this repository
2. Create a folder named _data_ in the root directory of the repository and copy the files _train.npz_, _valid.npz_ and _test.npz_ into the _data_ directory if not already there.
3. Run _run.py_, after this the prediction csv and trained models will be located inside of the _trained_models/generic_name_ folder

## inference / visual output
1. Locate your trained model 
2. Run _run_visualization.py_, after this 2 random sequences of human motion with its input and inference 


# Hints

- If model is trained on a different system (i.e. using a GPU) than the system you run _run_visualization.py_ on it won't work
- the currently configured model was the best w.r.t. the mean pose error metric which might not look the most natural and is also more feasible to run as an example (for more details about that I refer to the report.pdf)
