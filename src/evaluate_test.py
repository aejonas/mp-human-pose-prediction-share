import os
import tensorflow as tf
import numpy as np

from config import test_config, train_config
from visualize import visualize_joint_angles
from utils import export_to_csv, preprocessing_pipeline
from train import load_data, get_model_and_placeholders, get_encoder_model_and_placeholders


def main(config):
    # load the data
    data_test = load_data(config, 'test')

    # preprocessing of data
    if config['preprocessing']:
        data_train = load_data(train_config, 'train')
        preprocessing_pipeline_ = preprocessing_pipeline(config)

        # fit the transformations to the training input data
        input_training = np.concatenate(list(data_train.input_), axis=0)
        preprocessing_pipeline_.fit(input_training)

        # transform input and target for training and validation data with the fitted transformations
        for idx, _ in enumerate(data_test.input_):
            data_test.input_[idx] = preprocessing_pipeline_.transform(data_test.input_[idx])

    # build rnn (lstm) graph
    # dimensions (of input & output)
    if config['onehot_action_labels']:
        config['input_dim_lstm'] = data_test.input_[0].shape[-1] + 15
    else:
        config['input_dim_lstm'] = data_test.input_[0].shape[-1]

    if config['onehot_action_labels_encoder']:
        config['input_dim_encoder'] = data_test.input_[0].shape[-1] + 15
    else:
        config['input_dim_encoder'] = data_test.input_[0].shape[-1]

    config['output_dim'] = data_test.input_[0].shape[-1]

    print('input training shape lstm', config['input_dim_lstm'])
    print('input training shape encoder', config['input_dim_encoder'])
    print('output training shape', config['output_dim'])

    # build
    rnn_model, placeholders = get_model_and_placeholders(config)
    # restore the model by first creating the computational graph
    with tf.name_scope('inference'):
        rnn_model = rnn_model(config, placeholders, mode='inference')
        rnn_model.build_graph()

    # build encoder if config['lstm_encoder'] = True
    if config['lstm_encoder']:
        encoder_model, placeholders_encoder = get_encoder_model_and_placeholders(config)
        with tf.name_scope('inference_encoder'):
            encoder_model = encoder_model(config, placeholders_encoder, mode='inference')
            encoder_model.build_graph()

    # debugging
    if config['debugging']:
        print('variable names of all trainable variables:\n')
        for var in tf.trainable_variables():
            print(var.name)

    # make sure the Savor only tries to load the variable corresponding to it's trained model (in checkpoint)
    all_vars = tf.trainable_variables()
    lstm_vars = [k for k in all_vars if k.name.startswith('lstm')]
    dense_vars = [k for k in all_vars if k.name.startswith('encoder')]

    with tf.Session() as sess:
        # now restore the trained variables
        # this operation will fail if this `config` does not match the config you used during training
        saver = tf.train.Saver(lstm_vars)
        ckpt_id = config['checkpoint_id']

        print('ckpt_id: {}, model_dir: {}'.format(ckpt_id, config['model_dir']))

        if ckpt_id is None:
            print('its none and path: \n{}'.format(config['model_dir']))
            ckpt_path = tf.train.latest_checkpoint(config['model_dir'])
        else:
            ckpt_path = os.path.join(os.path.abspath(config['model_dir']), 'model-{}'.format(ckpt_id))
        print('Evaluating ' + ckpt_path)
        saver.restore(sess, ckpt_path)
        print('does it even work?')

        if config['lstm_encoder']:
            saver_encoder = tf.train.Saver(dense_vars)
            ckpt_id = config['checkpoint_id_encoder']
            if ckpt_id is None:
                ckpt_path = tf.train.latest_checkpoint(config['model_dir_encoder'])
            else:
                ckpt_path = os.path.join(os.path.abspath(config['model_dir_encoder']), 'model-{}'.format(ckpt_id))
            print('combined with ' + ckpt_path)
            saver_encoder.restore(sess, ckpt_path)

        # loop through all the test samples
        seeds = []
        predictions = []
        ids = []
        for batch in data_test.all_batches():

            # initialize the RNN with the known sequence (here 2 seconds)
            # no need to pad the batch because in the test set all batches have the same length
            input_onehot = np.array(batch.get_input_with_onehot())
            input_normal = np.array(batch.input_)
            if config['onehot_action_labels']:
                input_rnn = input_onehot
            else:
                input_rnn = input_normal

            seeds.append(input_rnn)

            # here we are requesting the final state as we later want to supply this back into the RNN
            # this is why the model should have a member `self.final_state`
            fetch = [rnn_model.final_state]
            feed_dict = {placeholders['input_pl']: input_rnn,
                         placeholders['seq_lengths_pl']: batch.seq_lengths}

            [state] = sess.run(fetch, feed_dict)

            # now get the prediction by predicting one pose at a time and feeding this pose back into the model to
            # get the prediction for the subsequent time step
            next_pose = input_rnn[:, -1:]
            onehot_arr = input_onehot[:, -1:, -15:]

            predicted_poses = []
            for f in range(config['prediction_length']):
                #   1) feed the previous final state of the model as the next initial state
                #   2) feed the previous output pose of the model as the new input (single frame only)
                #   3) fetch both the final state and prediction of the RNN model that are then re-used in the next
                #      iteration

                fetch = [rnn_model.final_state, rnn_model.prediction]

                # debugging
                # print('next_pose shape: {}'.format(next_pose.shape))

                feed_dict = {placeholders['input_pl']: next_pose,
                             placeholders['seq_lengths_pl']: [1 for _ in range(batch.batch_size)],
                             rnn_model.initial_state: state}
                [state, predicted_pose] = sess.run(fetch, feed_dict)

                if config['lstm_encoder']:

                    if config['onehot_action_labels_encoder']:
                        predicted_pose = np.append(predicted_pose, onehot_arr, 2)

                    fetch_encoder = [encoder_model.prediction]

                    feed_dict_encoder = {placeholders_encoder['input_pl']: predicted_pose,
                                         placeholders['seq_lengths_pl']: [1 for _ in range(batch.batch_size)]}
                    [predicted_pose] = sess.run(fetch_encoder, feed_dict_encoder)

                if config['onehot_action_labels']:
                    predicted_poses.append(next_pose[:, :, 0:next_pose.shape[2] - 15])
                    next_pose = np.append(predicted_pose, onehot_arr, 2)
                else:
                    predicted_poses.append(np.copy(next_pose))
                    next_pose = predicted_pose

            predicted_poses = np.concatenate(predicted_poses, axis=1)

            predictions.append(predicted_poses)
            ids.extend(batch.ids)

        if config['preprocessing_pipeline'] == 'remove_zeros':
            seeds = np.concatenate(seeds, axis=0)[:, :, 0:51]
        else:
            seeds = np.concatenate(seeds, axis=0)[:, :, 0:75]
        predictions = np.concatenate(predictions, axis=0)

    print('predictions and seeds before reshaping: ', predictions.shape, seeds.shape)

    # inverse transform predictions and input
    if config['preprocessing']:
        n_features = predictions.shape[-1]

        print('predictions and seeds before reshaping: ', predictions.shape, seeds.shape)
        predictions = predictions.reshape(-1, n_features)
        seeds = seeds.reshape(-1, n_features)

        print('predictions and seeds after reshaping: ', predictions.shape, seeds.shape)

        # transform input and target for training and validation data with the fitted transformations
        for idx, _ in enumerate(data_test.input_):
            data_test.input_[idx] = preprocessing_pipeline_.inv_transform(data_test.input_[idx])

        predictions = preprocessing_pipeline_.inv_transform(predictions)
        seeds = preprocessing_pipeline_.inv_transform(seeds)

        print('predictions and seeds after preprocessing: ', predictions.shape, seeds.shape)
        n_features = predictions.shape[-1]
        predictions = predictions.reshape(-1, test_config['prediction_length'], n_features)
        seeds = seeds.reshape(-1, train_config['max_seq_length'], n_features)

        print('predictions and seeds after preprocessing and reshaping: ', predictions.shape, seeds.shape)

    # the predictions are now stored in test_predictions, you can do with them what you want
    # for example, visualize a random entry
    idx = np.random.randint(0, len(seeds))
    seed_and_prediction = np.concatenate([seeds[idx], predictions[idx]], axis=0)
    visualize_joint_angles([seed_and_prediction], change_color_after_frame=seeds[0].shape[0])

    idx = np.random.randint(0, len(seeds))
    seed_and_prediction = np.concatenate([seeds[idx], predictions[idx]], axis=0)
    visualize_joint_angles([seed_and_prediction], change_color_after_frame=seeds[0].shape[0])

    # or, write out the test results to a csv file that you can upload to Kaggle
    model_name = config['model_dir'].split('/')[-1]
    model_name = config['model_dir'].split('/')[-2] if model_name == '' else model_name
    if config['lstm_encoder']:
        encoder_model_name = config['model_dir_encoder'].split('/')[-1]
        encoder_model_name = config['model_dir_encoder'].split('/')[
            -2] if encoder_model_name == '' else encoder_model_name
        output_file = os.path.join(config['model_dir'],
                                   'submit_to_kaggle_{}_{}_{}.csv'.format(config['prediction_length'], model_name,
                                                                          encoder_model_name))
    else:
        output_file = os.path.join(config['model_dir'],
                                   'submit_to_kaggle_{}_{}.csv'.format(config['prediction_length'], model_name))

    # postprocessing of data
    predictions[:, :, 7] = 0
    predictions[:, :, 8] = 0
    predictions[:, :, 13] = 0
    predictions[:, :, 14] = 0
    predictions[:, :, 19] = 0
    predictions[:, :, 20] = 0
    predictions[:, :, 25] = 0
    predictions[:, :, 26] = 0
    predictions[:, :, 46] = 0
    predictions[:, :, 47] = 0
    predictions[:, :, 51] = 0
    predictions[:, :, 52] = 0
    predictions[:, :, 53] = 0
    predictions[:, :, 54] = 0
    predictions[:, :, 55] = 0
    predictions[:, :, 56] = 0
    predictions[:, :, 64] = 0
    predictions[:, :, 65] = 0
    predictions[:, :, 69] = 0
    predictions[:, :, 70] = 0
    predictions[:, :, 71] = 0
    predictions[:, :, 72] = 0
    predictions[:, :, 73] = 0
    predictions[:, :, 74] = 0

    export_to_csv(predictions, ids, output_file)


if __name__ == '__main__':
    main(test_config)
