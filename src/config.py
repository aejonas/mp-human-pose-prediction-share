#### GENERAL PARAMETERS ###

train_config = {}
# directory for train/valid/test data
train_config['data_dir'] = '../data'
# output directory for checkpoints
train_config['output_dir'] = '../trained_models/'
# model name
train_config['name'] = 'lstm'
# debugging outputs on/off
train_config['debugging'] = False

#### TRAIN PARAMETERS ####

# after how many epochs the trained model should be saved
train_config['save_checkpoints_every_epoch'] = 3
# how many saved checkpoints to keep
train_config['n_keep_checkpoints'] = 5
# start with already trained model
train_config['warm_start'] = False
# path of already trained model
train_config['warm_start_model_dir'] = '../trained_models/'
# for final training use also the validation set to train
train_config['final_training'] = False
# True = encoder training, False = LSTM training
train_config['encoder'] = False

#### LEARNING PARAMETERS ####

# batch size to use
train_config['batch_size'] = 250
# number of epochs to train
train_config['n_epochs'] = 125
# how many time steps you want to unroll the RNN
train_config['max_seq_length'] = 50
# good (starting) learning rate so far is between 0.005 and 0.001
train_config['learning_rate'] = 0.004
# ['fixed', 'exponential', 'linear', 'adaptive_lin', 'adaptive_exp, 'schedule_lin', 'adaptive_exp]
train_config['learning_rate_type'] = 'exponential'  
# only used for exponential type
train_config['learning_rate_decay_steps'] = 100
# used as scaling during exponential type and as delta during linear
train_config['learning_rate_decay_rate'] = 0.85
# minimum allowed learning rate
train_config['minimal_learning_rate'] = 5e-5
# if scheduled learning is activated ( --> after ith entry epochs learning rate & dropout rate is updated)
train_config['learning_schedule'] = []

#### DROPOUT PARAMETERS ###

# ['fixed', 'exponential', 'linear', 'adaptive_lin', 'adaptive_exp, 'schedule_lin', 'adaptive_exp]
train_config['dropout_rate_type'] = 'adaptive_exp'
# only used for exponential type
train_config['dropout_rate_decay_steps'] = 100  
# used as scaling during exponential type and as delta during linear
train_config['dropout_rate_decay_rate'] = 0.85
# if scheduled learning is activated ( --> after ith entry epochs  dropout rate is updated)
train_config['dropout_schedule'] = []

train_config['flattening_threshold'] = 1e1  # if validation loss is flattening out (delta < flattening_threshold*learning_rate*validation_loss) adapt learning rate (good value so far 1e1)

#### DATA AUGMENTATION / PREPROCESSING ####

# use onehot action labels for lstm
train_config['onehot_action_labels'] = False
# use onehot action labels for encoder
train_config['onehot_action_labels_encoder'] = False
# include reversed sequences
train_config['use_reverse'] = False
# enable/disable preprocessing
train_config['preprocessing'] = False
# choose preprocessing functions: 'zero_one_range', 'standard', 'identity', 'remove_zeros' 
train_config['preprocessing_pipeline'] = ['identity']
# gaussian noise factor: noise stdev = factor * feature stdev
train_config['stdev_factor'] = 0.015


#### OPTIMIZER ####

# which optimizer to use
train_config['optimizer'] = 'adam'  # adam, momentum, ...
# adam beta 1
train_config['adam_beta1'] = 0.9
# adam beta 2
train_config['adam_beta2'] = 0.999
# epsilon
train_config['epsilon'] = 1e-08
# momentum optimizer's momentum
train_config['momentum'] = 0.9
# gradient clipping: if < 0 there is no clipping if > 0 it clips to the given value
train_config['gradient_clipping'] = -1

#### LSTM PARAMETERS ####

# use deep lstm or not
train_config['deep_lstm'] = False
# lstm layers & their number of units
train_config['n_hidden_rnn'] = [400]
# dense layers & their number of units (dense layers are located after lstm)
# no layers specifiec => one dense layer after lstm that has 75 outputs
train_config['n_hidden_dense'] = []  
# dropout rate lstm
train_config['dropout_rate_rnn'] = 0.4
# dropout rate dense
train_config['dropout_rate_dense'] = 0.4
# use pre dense or not (pre dense layers are located before lstm)
train_config['pre_dense_layer'] = False
# pre dense layers & their number of units
train_config['n_hidden_pre_dense'] = [100, 100]

#### ENCODER PARAMETERS ####

# encoder layers & their number of units (additional layer with 75 output added automatically)
train_config['n_hidden_encoder'] = [400, 400, 400]
# dropout rate encoder
train_config['dropout_rate_encoder'] = 0.05
# activation function of encoder
train_config['activation'] = 'relu'
# l2 regularisation thr
train_config['l2_regularization'] = 1e-7

#### AUTOMATIC CONFIG ADJUSTMENTS ####

if train_config['encoder']:
    # encoder name
    train_config['name'] = 'encoder'
    # seq length for encoder is always 1
    train_config['max_seq_length'] = 1
    # configuration used by the training and evaluation scripts
    print('\nENCODER TRAINING\nsequence length is automatically set to 1\n')
    
if train_config['deep_lstm']:
    #dont use onehot
    train_config['onehot_action_labels'] = False


#### INFERENCE PARAMETERS ####

test_config = train_config.copy()
# want to use entire sequence during test, which is fixed to 50, don't change this
test_config['max_seq_length'] = -1
# trained LSTM model directory
test_config['model_dir'] = '../trained_models/'
# if None, the last checkpoint will be used
test_config['checkpoint_id'] = None
# how many frames to predict
test_config['prediction_length'] = 25

# if encoder is used in combination with lstm
test_config['lstm_encoder'] = False  # evaluate test set on lstm cleaning output with encoder (hilliges et. al.)
# trained encoder model directory
test_config['model_dir_encoder'] = '../trained_models/'
# if None, the last checkpoint will be used
test_config['checkpoint_id_encoder'] = None


#### VISUALISATION PARAMETERS ####

visualize_config = test_config.copy()
# we want to have 50 seed and 25 prediciton
visualize_config['max_seq_length'] = 75
