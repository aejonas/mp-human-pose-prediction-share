from train import *
from config import train_config
from importlib import reload

# path to config.py
file_path = '/home/aejonas/Documents/MP18project/Skeleton/src/config.py'
#file_path = 'config.py'

# store content config.py in config_content
config_content = []

print('started exploration\n')

learning_rates = [0.01, 0.005, 0.001]
decay_rates = [0.75]
thresholds = [1e0, 1e1, 1e2]
n_hidden_denses = [[200, 200], [400, 400], [600, 600], [1000, 1000]]
dropout_dense = [0.5]

train_config['encoder'] = True
train_config['onehot_input'] = False  # oh
train_config['preprocessing_pipeline'] = ['identity']  #pr-id, pr-std, pr-zor
train_config['preprocessing'] = False
train_config['n_epoch'] = 75

name_partial = 'encoder'
if train_config['onehot_action_labels']:
    name_partial += '_oh'

if train_config['preprocessing_pipeline'] == 'standard':
    name_partial += '_pr-std'
elif train_config['preprocessing_pipeline'] == 'zero-one_range':
    name_partial += '_pr-zor'


# iterate over all configurations and train the model
for learning_rate in learning_rates:
    for decay_rate in decay_rates:
        for threshold in thresholds:
            for n_hidden_dense in n_hidden_denses:
                for dropout in dropout_dense:


                        # change learning rate
                        train_config['learning_rate'] = learning_rate
                        # change learning_rate_decay_rate
                        train_config['learning_rate_decay_rate'] = decay_rate
                        # change flattening_threshold
                        train_config['flattening_threshold'] = threshold
                        # change n_hidden_dense
                        train_config['n_hidden_encoder'] = n_hidden_dense
                        # change dropout_rate_dense
                        train_config['dropout_rate_encoder'] = dropout

                        n_hidden_dense_name = '-'.join(['%-2s' % (i,) for i in n_hidden_dense]) + '-75'

                        # change name (format: lstm_n-hidden-rnn_n-hidden-dense_dropouts_learning-rate-decay-rate_flattening-threshold)
                        name = '{}_{}_{}_{}_{}'.format(name_partial, n_hidden_dense_name, dropout, decay_rate, threshold)
                        train_config['name'] = name

                        print('\nstart training:\n')
                        print(name, '\n')

                        if __name__ == '__main__':
                            main(train_config)





