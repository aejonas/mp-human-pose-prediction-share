import datetime
import os
import tensorflow as tf
import time
import numpy as np
from config import train_config
from encoder import EncoderModel
from load_data import MotionDataset
from utils import export_config, preprocessing_pipeline
if train_config['deep_lstm']:
    from model_complex import RNNModel
else:
    from model import RNNModel


def load_data(config, split):
    print('Loading data from {} ...'.format(config['data_dir']))
    return MotionDataset.load(data_path=config['data_dir'],
                              split=split,
                              seq_length=config['max_seq_length'],
                              batch_size=config['batch_size'],
                              use_reverse=config['use_reverse'],
                              onehot_action_labels=config['onehot_action_labels'] and not config['encoder'] or config['onehot_action_labels_encoder'] and config['encoder'])


def get_model_and_placeholders(config):
    # create placeholders that we need to feed the required data into the model
    # None means that the dimension is variable, which we want for the batch size and the sequence length
    #input_dim = output_dim = config['input_dim']
    input_dim = config['input_dim_lstm']
    output_dim = config['output_dim']

    input_pl = tf.placeholder(tf.float32, shape=[None, None, input_dim], name='input_pl')
    target_pl = tf.placeholder(tf.float32, shape=[None, None, output_dim], name='input_pl')
    seq_lengths_pl = tf.placeholder(tf.int32, shape=[None], name='seq_lengths_pl')
    mask_pl = tf.placeholder(tf.float32, shape=[None, None], name='mask_pl')
    action_mask_pl = tf.placeholder(tf.float32, shape=[None, None], name='action_mask_pl')
    dropout_rate_rnn_pl = tf.placeholder(tf.float32, shape=[], name='dropout_rate_rnn_pl')
    dropout_rate_dense_pl = tf.placeholder(tf.float32, shape=[], name='dropout_rate_dense_pl')

    placeholders = {'input_pl': input_pl,
                    'target_pl': target_pl,
                    'seq_lengths_pl': seq_lengths_pl,
                    'mask_pl': mask_pl,
                    'action_mask_pl': action_mask_pl,
                    'dropout_rate_rnn_pl': dropout_rate_rnn_pl,
                    'dropout_rate_dense_pl': dropout_rate_dense_pl}

    rnn_model_class = RNNModel
    return rnn_model_class, placeholders


def get_encoder_model_and_placeholders(config):
    #input_dim = output_dim = config['input_dim']
    input_dim = config['input_dim_encoder']
    output_dim = config['output_dim']
    input_pl = tf.placeholder(tf.float32, shape=[None, None, input_dim], name='input_pl')
    target_pl = tf.placeholder(tf.float32, shape=[None, None, output_dim], name='input_pl')
    seq_lengths_pl = tf.placeholder(tf.int32, shape=[None], name='seq_lengths_pl')
    mask_pl = tf.placeholder(tf.float32, shape=[None, None], name='mask_pl')
    action_mask_pl = tf.placeholder(tf.float32, shape=[None, None], name='action_mask_pl')
    dropout_rate_pl = tf.placeholder(tf.float32, shape=[], name='dropout_rate_pl')

    placeholders = {'input_pl': input_pl,
                    'target_pl': target_pl,
                    'seq_lengths_pl': seq_lengths_pl,
                    'mask_pl': mask_pl,
                    'action_mask_pl': action_mask_pl,
                    'dropout_rate_pl': dropout_rate_pl}

    encoder_model_class = EncoderModel
    return encoder_model_class, placeholders


def main(config):
    tf.reset_default_graph()
    # create unique output directory for this model
    timestamp = str(int(time.time()))
    config['model_dir'] = os.path.abspath(os.path.join(config['output_dir'], config['name']))
    os.makedirs(config['model_dir'])
    print('Writing checkpoints into {}'.format(config['model_dir']))

    # load the data, this requires that the *.npz files you downloaded from Kaggle be named `train.npz` and `valid.npz`
    data_train = load_data(config, 'train')
    data_valid = load_data(config, 'valid')

    # use validation and training set at your final traning stage
    if config['final_training']:
        data_train.input_.extend(data_valid.input_)
        data_train.target.extend(data_valid.target)
        data_train.ids.extend(data_valid.ids)
        data_train.action_labels.extend(data_valid.action_labels)
        data_train.set_n_batches()

    # TODO if you would like to do any preprocessing of the data, here would be a good opportunity can still add more (in utils.py)
    # as far as I understand the data is already preprocessed up to a point where
    # the skeleton size is normalized and only the relative joint angles are taken as input
    # but features are not yet in range [0,1] (for each feature separately) (Hilliges paper chapter 4.2)

    # preprocessing of data
    if config['preprocessing']:
        print('\nsending data through preprocessing pipeline ...')
        preprocessing_pipeline_ = preprocessing_pipeline(config)

        # fit the transformations to the training input data
        input_training = np.concatenate(list(data_train.input_), axis=0)
        preprocessing_pipeline_.fit(input_training)

        # transform input and target for training and validation data with the fitted transformations
        for idx, _ in enumerate(data_train.input_):
            data_train.input_[idx] = preprocessing_pipeline_.transform(data_train.input_[idx])
            data_train.target[idx] = preprocessing_pipeline_.transform(data_train.target[idx])

        for idx, _ in enumerate(data_valid.input_):
            data_valid.input_[idx] = preprocessing_pipeline_.transform(data_valid.input_[idx])
            data_valid.target[idx] = preprocessing_pipeline_.transform(data_valid.target[idx])
        print('data preprocessed\n')

    if config['onehot_action_labels']:
        config['input_dim_lstm'] = data_train.input_[0].shape[-1] + 15
    else:
        config['input_dim_lstm'] = data_train.input_[0].shape[-1]
        
    config['output_dim'] = data_train.input_[0].shape[-1]
    
    if config['onehot_action_labels_encoder']:
        config['input_dim_encoder'] = data_train.input_[0].shape[-1] + 15
    else:
        config['input_dim_encoder'] = data_train.input_[0].shape[-1]

    if config['encoder']:
        config['input_dim'] = config['input_dim_encoder']
    else:
        config['input_dim'] = config['input_dim_lstm']
        
    print('input dimension ', config['input_dim'], '\n', 'output dimension ', config['output_dim'])
        
    # calculate feature standard deviations
    data_concatenated = np.concatenate(list(data_train.input_), axis=0)
    feature_stds = np.asarray(np.matrix.std(np.matrix(data_concatenated),0))[0]
    config['feature_stds'] = np.multiply(feature_stds, config['stdev_factor'])

    # get input placeholders and get the model that we want to train
    if config['encoder']:
        model_class, placeholders = get_encoder_model_and_placeholders(config)
    else:
        model_class, placeholders = get_model_and_placeholders(config)
          
    # Create a variable that stores how many training iterations we performed.
    # This is useful for saving/storing the network
    global_step = tf.Variable(1, name='global_step', trainable=False)

    # create a training graph, this is the graph we will use to optimize the parameters
    with tf.name_scope('training'):
        model = model_class(config, placeholders, mode='training')
        model.build_graph()
        if config['encoder']:
            print('created encoder model with {} parameters'.format(model.n_parameters))
        else:
            print('created RNN model with {} parameters'.format(model.n_parameters))

        # configure learning rate
        if config['learning_rate_type'] == 'fixed':
            lr = config['learning_rate']
            lr_decay_op = tf.identity(lr)
        elif config['learning_rate_type'] == 'linear':
            lr = tf.Variable(config['learning_rate'], trainable=False)
            #lr_decay_op = lr.assign(tf.multiply(lr, config['learning_rate_decay_rate']))
            lr_decay_op = lr.assign(tf.maximum(tf.subtract(lr, config['learning_rate_decay_rate']), config['minimal_learning_rate']))
        elif config['learning_rate_type'] in ['adaptive_lin', 'schedule_lin']:
            lr = tf.Variable(config['learning_rate'], trainable=False)
            lr_decay_op = lr.assign(tf.maximum(tf.subtract(lr, config['learning_rate_decay_rate']), config['minimal_learning_rate']))
        elif config['learning_rate_type'] == 'exponential':
            lr = tf.train.exponential_decay(config['learning_rate'],
                                            global_step=global_step,
                                            decay_steps=config['learning_rate_decay_steps'],
                                            decay_rate=config['learning_rate_decay_rate'],
                                            staircase=True)
            lr_decay_op = tf.identity(lr)
        elif config['learning_rate_type'] in ['adaptive_exp', 'schedule_exp']:
            lr = tf.Variable(config['learning_rate'], trainable=False)
            lr_decay_op = lr.assign(tf.maximum(tf.multiply(lr, config['learning_rate_decay_rate']), config['minimal_learning_rate']))
        else:
            raise ValueError('learning rate type "{}" unknown.'.format(config['learning_rate_type']))

        # todo: currently all dropouts are updated the same (even if there are rnn/dense/... different networks) --> use dr as scaling
        # todo: which means that dr_0 = 1 and dropout_rate_rnn/dense will be scaled using this factor
        # initialize the operator and variable for the dropout rate (with annealling schedule given)
        if config['dropout_rate_type'] == 'fixed':
            dr = 1.0
            dr_decay_op = tf.identity(dr)
        # linear annealed dropout (p - p/n_epochs)
        elif config['dropout_rate_type'] == 'linear':
            dr = tf.Variable(1.0, trainable=False)
            dr_decay_op = dr.assign(tf.maximum(0.0, tf.subtract(dr, 1.0/train_config['n_epochs'])))
        # adaptive linear annealed dropout (p - dropout_rate_decay_rate)
        elif config['dropout_rate_type'] == 'adaptive_lin':
            dr = tf.Variable(1.0, trainable=False)
            dr_decay_op = dr.assign(tf.maximum(0.0, tf.subtract(dr, config['dropout_rate_decay_rate'])))
        # scheduled linear annealed dropout (p - dropout_rate_decay_rate)
        elif config['dropout_rate_type'] == 'schedule_lin':
            dr = tf.Variable(1.0, trainable=False)
            dr_decay_op = dr.assign(tf.subtract(dr, config['dropout_rate_decay_rate']))
        # exponential annealed dropout (p * dropout_rate_decay_rate)
        elif config['dropout_rate_type'] == 'exponential':
            dr = tf.train.exponential_decay(1.0,
                                            global_step=global_step,
                                            decay_steps=config['dropout_rate_decay_steps'],
                                            decay_rate=config['dropout_rate_decay_rate'],
                                            staircase=True)
            dr_decay_op = tf.identity(dr)
        # scheduled/adaptive exponential annealed dropout (p * dropout_rate_decay_rate)
        elif config['dropout_rate_type'] in ['adaptive_exp', 'schedule_exp']:
            #dr = tf.Variable(config['dropout_rate'], trainable=False)
            dr = tf.Variable(1.0, trainable=False)
            dr_decay_op = dr.assign(tf.multiply(dr, config['dropout_rate_decay_rate']))
        else:
            raise ValueError('dropout rate type "{}" unknown.'.format(config['dropout_rate_type']))

        # TODO choose the optimizer you desire here and define `train_op. The loss should be accessible through model.loss
        if config['optimizer'] == 'adam':
            optimizer = tf.train.AdamOptimizer(learning_rate=lr,
                                               beta1=config['adam_beta1'],
                                               beta2=config['adam_beta2'],
                                               epsilon=config['epsilon'])
        elif config['optimizer'] == 'momentum':
            optimizer = tf.train.MomentumOptimizer(learning_rate=lr,
                                                   momentum=config['momentum']).minimize(model.loss)

        # get the gradients
        params = tf.trainable_variables()
        gradients = tf.gradients(model.loss, params)

        # clip the gradients to counter explosion
        if config['gradient_clipping'] > 0:
            clipped_gradients, _ = tf.clip_by_global_norm(gradients, config['gradient_clipping'])
        else:
            clipped_gradients = gradients

        # backpropagation
        train_op = optimizer.apply_gradients(zip(clipped_gradients, params), global_step=global_step)

    # create a graph for validation
    with tf.name_scope('validation'):
        model_valid = model_class(config, placeholders, mode='validation')
        model_valid.build_graph()

    # Create summary ops for monitoring the training
    # Each summary op annotates a node in the computational graph and collects data data from it
    tf.summary.scalar('learning_rate', lr, collections=['training_summaries'])

    # Merge summaries used during training and reported after every step
    summaries_training = tf.summary.merge(tf.get_collection('training_summaries'))

    # create summary ops for monitoring the validation
    # caveat: we want to store the performance on the entire validation set, not just one validation batch
    # Tensorflow does not directly support this, so we must process every batch independently and then aggregate
    # the results outside of the model
    # so, we create a placeholder where can feed the aggregated result back into the model
    loss_valid_pl = tf.placeholder(tf.float32, name='loss_valid_pl')
    loss_valid_s = tf.summary.scalar('loss_valid', loss_valid_pl, collections=['validation_summaries'])

    # merge validation summaries
    summaries_valid = tf.summary.merge([loss_valid_s])

    # dump the config to the model directory in case we later want to see it
    export_config(config, os.path.join(config['model_dir'], 'config.txt'))

    # debugging
    if config['debugging']:
        print('variable names of all trainable variables:\n')
        for var in tf.trainable_variables():
            print(var.name)

        print('\nvariable names of all variables:\n')
        for var in tf.global_variables():
            print(var.name)

    # Variables used for computing the annealed dropout of the different network parts
    dropout_update_encoder = tf.Variable(config['dropout_rate_encoder'], trainable=False, name='dropout_update_encoder')
    dropout_update_encoder_op = dropout_update_encoder.assign(tf.multiply(config['dropout_rate_encoder'], dr))
    dropout_update_rnn = tf.Variable(config['dropout_rate_rnn'], trainable=False, name='dropout_update_rnn')
    dropout_update_rnn_op = dropout_update_rnn.assign(tf.multiply(config['dropout_rate_rnn'], dr))
    dropout_update_dense = tf.Variable(config['dropout_rate_dense'], trainable=False, name='dropout_update_dense')
    dropout_update_dense_op = dropout_update_dense.assign(tf.multiply(config['dropout_rate_dense'], dr))

    with tf.Session() as sess:
        # Add the ops to initialize variables.
        init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
        # Actually intialize the variables
        sess.run(init_op)

        # if warm start is True load an already trained model (from given path) and continue training
        all_vars = tf.trainable_variables()
        if config['warm_start']:
            saver = tf.train.Saver(all_vars)
            #ckpt_id = config['checkpoint_id']
            ckpt_id = None
            if ckpt_id is None:
                ckpt_path = tf.train.latest_checkpoint(config['warm_start_model_dir'])
            else:
                ckpt_path = os.path.join(os.path.abspath(config['warm_start_model_dir']), 'model-{}'.format(ckpt_id))
            print('\nloading learned weights from: ' + ckpt_path, '\n')
            saver.restore(sess, ckpt_path)

        # create file writers to dump summaries onto disk so that we can look at them with tensorboard
        train_summary_dir = os.path.join(config['model_dir'], "summary", "train")
        train_summary_writer = tf.summary.FileWriter(train_summary_dir, sess.graph)
        valid_summary_dir = os.path.join(config['model_dir'], "summary", "validation")
        valid_summary_writer = tf.summary.FileWriter(valid_summary_dir, sess.graph)

        # create a saver for writing training checkpoints
        saver = tf.train.Saver(var_list=tf.trainable_variables(), max_to_keep=config['n_keep_checkpoints'])

        # start training
        start_time = time.time()
        current_step = 0
        # initialize validation loss memory for convergence test
        validation_loss = [1.0e14, 1.0e5]
        flattening_threshold = config['flattening_threshold']
        #consecutive = False
        consecutive = True

        for e in range(config['n_epochs']):
            # reshuffle the batches
            data_train.reshuffle()

            # loop through all training batches
            for i, batch in enumerate(data_train.all_batches()):
                step = tf.train.global_step(sess, global_step)
                current_step += 1

                # debugging
                if config['debugging']:
                    print('\n\nTRAINING STEP NR. ----------------------   ', current_step, ' --- EPOCH ', e, '\n')

                # if linear learning rate type is used check if conditions are met
                # and update the learning rate
                if config['learning_rate_type'] == 'linear' and current_step % config['learning_rate_decay_steps'] == 0:
                    sess.run(lr_decay_op)

                # if dropout learning rate type is used check if conditions are met
                # and update the learning rate
                if config['dropout_rate_type'] == 'exponential' and current_step % config['dropout_rate_decay_steps'] == 0:
                    sess.run(dr_decay_op)
                    sess.run(dropout_update_encoder_op)
                    sess.run(dropout_update_rnn_op)
                    sess.run(dropout_update_dense_op)

                # we want to train, so must request at least the train_op
                fetches = {'summaries': summaries_training,
                           'loss': model.loss,
                           'train_op': train_op}

                # get the feed dict for the current batch
                if config['encoder']:
                    feed_dict = model.get_feed_dict(
                        batch, 
                        sess.run(dropout_update_encoder),
                        config['onehot_action_labels_encoder'])
                elif config['deep_lstm']:
                    feed_dict = model.get_feed_dict(
                        batch, 
                        sess.run(dropout_update_rnn), 
                        sess.run(dropout_update_dense))
                else:
                    feed_dict = model.get_feed_dict(
                        batch, 
                        sess.run(dropout_update_rnn), 
                        sess.run(dropout_update_dense),
                        config['onehot_action_labels'])

                # feed data into the model and run optimization
                training_out = sess.run(fetches, feed_dict)

                # write logs
                train_summary_writer.add_summary(training_out['summaries'], global_step=step)

                # print training performance of this batch onto console
                time_delta = str(datetime.timedelta(seconds=int(time.time() - start_time)))
                print('\rEpoch: {:3d} [{:4d}/{:4d}] time: {:>8} loss: {:.4f}'.format(
                    e + 1, i + 1, data_train.n_batches, time_delta, training_out['loss']))


            # debugging (show all weights and bias')
            if config['debugging']:
                print('all kernels during validation phase')
                for v in tf.trainable_variables():
                    print(v.name, '\n', sess.run(v))


            # after every epoch evaluate the performance on the validation set
            total_valid_loss = 0.0
            n_valid_samples = 0
            for batch in data_valid.all_batches():

                # debugging
                if config['debugging']:
                    print('\n\nVALIDATION STEP ----------------------   \n')

                fetches = {'loss': model_valid .loss}
                if config['encoder']:
                    feed_dict = model_valid .get_feed_dict(batch, onehot=config['onehot_action_labels_encoder'])
                elif config['deep_lstm']:
                    feed_dict = model_valid .get_feed_dict(batch)
                else:
                    feed_dict = model_valid .get_feed_dict(batch, onehot=config['onehot_action_labels'])

                valid_out = sess.run(fetches, feed_dict)

                total_valid_loss += valid_out['loss'] * batch.batch_size
                n_valid_samples += batch.batch_size

            # if adaptive / schedule learning rate type is used check if conditions are met
            # and update the learning rate
            delta = abs(validation_loss[-1] - validation_loss[-2])
            flattened = abs(delta) < flattening_threshold * sess.run(lr) * validation_loss[-1]
            if (config['learning_rate_type'] in ['adaptive_lin', 'adaptive_exp'] and flattened and consecutive) \
                    or (config['learning_rate_type'] in ['schedule_lin', 'schedule_exp'] and e in config['learning_schedule']):
                sess.run(lr_decay_op)


            # if adaptive / schedule dropout rate type is used check if conditions are met
            # if linear dropout rate type always do it (after each epoch)
            # and update the learning rate (do this separately for the different network parts)
            if (config['dropout_rate_type'] in ['adaptive_lin', 'adaptive_exp'] and flattened and consecutive) \
                    or (config['dropout_rate_type'] in ['schedule_lin', 'schedule_exp'] and e in config['dropout_schedule'])\
                    or (config['dropout_rate_type'] == 'linear'):
                sess.run(dr_decay_op)
                sess.run(dropout_update_encoder_op)
                sess.run(dropout_update_rnn_op)
                sess.run(dropout_update_dense_op)

            # to result as a flattened validation curve the performance has to be similar for two consecutive epochs
            if flattened:
                consecutive = True
            else:
                #consecutive = False
                consecutive = True

            # write validation logs
            avg_valid_loss = total_valid_loss / n_valid_samples
            valid_summaries = sess.run(summaries_valid, {loss_valid_pl: avg_valid_loss})
            valid_summary_writer.add_summary(valid_summaries, global_step=tf.train.global_step(sess, global_step))

            # print validation performance onto console
            print(' | validation loss: {:.6f}'.format(avg_valid_loss))

            # for convergence test
            validation_loss.append(avg_valid_loss)

            # save this checkpoint if necessary
            if (e + 1) % config['save_checkpoints_every_epoch'] == 0:
                saver.save(sess, os.path.join(config['model_dir'], 'model'), global_step)

        # Training finished, always save model before exiting
        print('Training finished')
        ckpt_path = saver.save(sess, os.path.join(config['model_dir'], 'model'), global_step)
        print('Model saved to file {}'.format(ckpt_path))


if __name__ == '__main__':
    main(train_config)
