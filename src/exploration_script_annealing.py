from train import *
from config import train_config

print('started exploration\n')

# exploring configurations
learning_rates = [0.004]
#annealing_rate_types = ['fixed', 'exponential', 'linear', 'adaptive_lin', 'adaptive_exp', 'schedule_lin', 'schedule_exp']
annealing_rate_types = [['linear', 'linear'], ['linear', 'exponential'], ['linear', 'adaptive_lin'], ['linear', 'adaptive_exp'],
                        ['adaptive_exp', 'linear'], ['adaptive_exp', 'exponential'], ['adaptive_exp', 'adaptive_lin'], ['adaptive_exp', 'adaptive_exp']]

n_hidden_rnns = [[400], [600, 600], [800, 800, 800]]
n_hidden_dense = [[], [], []]
n_hidden_pre_dense = [[], [], [200]]
pre_dense_layer = [False, False, True]

# fixed configuration

train_config['encoder'] = False
train_config['preprocessing_pipeline'] = ['identity']  #pr-id, pr-std, pr-zor
train_config['preprocessing'] = False
train_config['n_epochs'] = 100
train_config['onehot_action_labels'] = False # oh
train_config['final_training'] = False
train_config['use_reverse'] = False
train_config['learning_schedule'] = [5, 10, 11, 20, 30, 45, 46, 47]
train_config['dropout_schedule'] = [5, 10, 11, 20, 30, 45, 46, 47]
train_config['warm_start'] = False

train_config['learning_rate_decay_steps'] = 50  # only used for exponential type
#train_config['learning_rate_decay_rate'] = 0.5  # used as scaling during exponential type and as delta during linear
train_config['minimal_learning_rate'] = 1e-6
train_config['dropout_rate_decay_steps'] = 50  # only used for exponential type
#train_config['dropout_rate_decay_rate'] = 0.5
train_config['flattening_threshold'] = 1e1

train_config['dropout_rate_rnn'] = 0.4  # dropout-layer at each LSTM layer
train_config['dropout_rate_dense'] = 0.5  # dropout-layer at the input of the dense layers




# iterate over all configurations and train the model
for architecture_id, n_hidden_rnn in enumerate(n_hidden_rnns):
    for annealing_rate_type in annealing_rate_types:

        name_partial = 'lstm_annealing_test'

        if annealing_rate_type[0] in ['linear']:
            train_config['learning_rate_decay_rate'] = 0.0002
            name_partial += '_lr-lin'
        elif annealing_rate_type[0] in ['adaptive_exp']:
            train_config['learning_rate_decay_rate'] = 0.75
            name_partial += '_lr-adexp'

        if annealing_rate_type[1] in ['linear', 'adaptive_lin']:
            train_config['dropout_rate_decay_rate'] = 0.005
            if annealing_rate_type[1] == 'linear':
                name_partial += '_dr-lin'
            elif annealing_rate_type[1] == 'adaptive_lin':
                name_partial += '_dr-adlin'
        elif annealing_rate_type[1] in ['exponential', 'adaptive_exp']:
            train_config['dropout_rate_decay_rate'] = 0.75
            if annealing_rate_type[1] == 'exponential':
                name_partial += '_dr-exp'
            elif annealing_rate_type[1] == 'adaptive_exp':
                name_partial += '_dr-adexp'


        # change learning rate type
        train_config['learning_rate_type'] = annealing_rate_type[0]
        # change dropout rate type
        train_config['dropout_rate_type'] = annealing_rate_type[1]
        # change number of hidden states in lstm
        train_config['n_hidden_rnn'] = n_hidden_rnn
        # change number of hidden units in dense
        train_config['n_hidden_dense'] = n_hidden_dense[architecture_id]
        # change number of hidden units in pre dense
        train_config['n_hidden_pre_dense'] = n_hidden_pre_dense[architecture_id]
        # change use of pre dense layer
        train_config['pre_dense_layer'] = pre_dense_layer[architecture_id]

        if train_config['pre_dense_layer']:
            n_hidden_pre_dense_name = '-'.join(['%-2s' % (i,) for i in train_config['n_hidden_pre_dense']])
            name_partial += '_pd-{}'.format(n_hidden_pre_dense_name)

        n_hidden_rnn_name = '-'.join(['%-2s' % (i,) for i in train_config['n_hidden_rnn']])
        n_hidden_dense_name = '-'.join(['%-2s' % (i,) for i in train_config['n_hidden_dense']]) + '-75'


        # change name (format: lstm_name_n-pre-dense_n-hidden-rnn_n-hidden-dense)
        name = '{}_{}_{}'.format(name_partial, n_hidden_rnn_name, n_hidden_dense_name)
        train_config['name'] = name

        print('\nstart training:\n')
        print(name, '\n')

        if __name__ == '__main__':
            main(train_config)





