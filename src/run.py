from train import *
import tensorflow as tf
from train import main as train
from evaluate_test import *
from evaluate_test import main as eval
from config import test_config, train_config

train_config['name'] = 'lstm_' + str(int(time.time()))

print('\nstart training:')
print(train_config['name'], '\n')


print('\ntraining run 1')
train(train_config)

print('\nadapting config for run 2')

train_config['warm_start'] = True
train_config['warm_start_model_dir'] = '../trained_models/' + train_config['name'] + '/'
train_config['name'] = train_config['name'] + '_2'
train_config['final_training'] = True
train_config['n_epochs'] = 75
train_config['learning_rate'] = 0.00075
train_config['dropout_rate_type'] = 'exponential'
train_config['stdev_factor'] = 0.0022
train_config['dropout_rate_rnn'] = 0.075
train_config['dropout_rate_dense'] = 0.075

print('\ntraining run 2')
tf.reset_default_graph()
train(train_config)

print('\nstart evaluating:')

test_config['model_dir'] = '../trained_models/' + train_config['name'] + '/'
print(train_config['model_dir'])
print(test_config['model_dir'])

tf.reset_default_graph()
eval(test_config)

print('The result csv is located at: ' + test_config['model_dir'])