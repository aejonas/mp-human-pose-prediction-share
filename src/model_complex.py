import tensorflow as tf


class RNNModel(object):
    """
    Creates training and validation computational graphs.
    Note that tf.variable_scope enables parameter sharing so that both graphs are identical.
    """

    def __init__(self, config, placeholders, mode):
        """
        Basic setup.
        :param config: configuration dictionary
        :param placeholders: dictionary of input placeholders
        :param mode: training, validation or inference
        """
        assert mode in ['training', 'validation', 'inference']
        assert len(config['n_hidden_rnn']) == 3, 'config[n_hidden_rnn] is not a valid input'
        self.config = config
        self.input_ = placeholders['input_pl']
        self.target = placeholders['target_pl']
        self.mask = placeholders['mask_pl']
        self.seq_lengths = placeholders['seq_lengths_pl']
        self.mode = mode
        self.is_training = self.mode == 'training'
        self.reuse = self.mode == 'validation'
        self.batch_size = tf.shape(self.input_)[0]  # dynamic size
        self.max_seq_length = tf.shape(self.input_)[1]  # dynamic size
        self.input_dim = config['input_dim_lstm']
        self.output_dim = config['output_dim']
        self.summary_collection = 'training_summaries' if mode == 'training' else 'validation_summaries'

        # newly added variables
        self.n_hidden_rnn_shallow = config['n_hidden_rnn'][0]  # dimensions for the shallow first part of the network
        self.n_hidden_rnn_medium = config['n_hidden_rnn'][1]  # dimensions for the medium second part of the network
        self.n_hidden_rnn_deep = config['n_hidden_rnn'][2]  # dimensions for the deep third part of the network
        self.dropout_rate_rnn = config['dropout_rate_rnn']
        self.dropout_rate_dense = config['dropout_rate_dense']
        self.n_hidden_dense = config['n_hidden_dense']
        self.dropout_rate_rnn_pl = placeholders['dropout_rate_rnn_pl']
        self.dropout_rate_dense_pl = placeholders['dropout_rate_dense_pl']

        self.pre_dense_layer = config['pre_dense_layer']
        self.n_hidden_pre_dense = config['n_hidden_pre_dense']

        self.debugging = config['debugging']

    def build_graph(self):
        self.build_model()
        self.build_loss()
        self.count_parameters()

    def build_model(self):
        """
        Builds the actual model.
        """
        # ----- LSTM CELLS (MULTIPLE LAYERS IF CONFIGURED) ----- #
        with tf.variable_scope('lstm', reuse=self.reuse):

            tf.summary.scalar('dropout_rate_rnn', self.dropout_rate_rnn_pl, collections=[self.summary_collection])
            tf.summary.scalar('dropout_rate_dense', self.dropout_rate_dense_pl, collections=[self.summary_collection])

            # todo maybe need to set forget_bias = 0.0 during inference phase

            # shallow part of network
            cell_shallow = self.build_lstm_cell(self.n_hidden_rnn_shallow, 'shallow')

            # medium part of network
            cell_medium = self.build_lstm_cell(self.n_hidden_rnn_medium, 'medium')

            # deep part of network
            cell_deep = self.build_lstm_cell(self.n_hidden_rnn_deep, 'deep')

            # we need to set an initial state for the cells
            batch_size = tf.shape(self.input_)[0]

            self.initial_state = tuple([cell_shallow.zero_state(batch_size, dtype=tf.float32),
                                        cell_medium.zero_state(batch_size, dtype=tf.float32),
                                        cell_deep.zero_state(batch_size, dtype=tf.float32)])

            # debugging
            if not self.mode == 'inference' and self.debugging:
                input_print = tf.Print(self.input_, [self.input_[0, 1, 0], self.input_[0, 2, 0], self.input_[0, 3, 0],
                                                     self.input_[0, 1, 40], self.input_[0, 2, 40],
                                                     self.input_[0, 3, 40],
                                                     tf.shape(self.input_)], 'input (and shape)\n')

                input_print = tf.Print(input_print,
                                       [input_print[0, 0, 75:78], input_print[0, 0, 78:81], input_print[0, 0, 81:84],
                                        input_print[0, 0, 85:88], input_print[0, 0, 88:]],
                                       'input one-hot action label\n')
            else:
                input_print = self.input_

            # use a dense net in front of lstm to learn features first and then learn sequence
            if self.pre_dense_layer:
                input_print = tf.reshape(input_print, [-1, self.input_dim])

                pre_dense = tf.layers.dropout(inputs=input_print, rate=self.dropout_rate_dense_pl,
                                              training=self.is_training)
                for id_, n_hidden in enumerate(self.n_hidden_pre_dense):
                    tf.summary.histogram('pre dense {}'.format(id_), pre_dense, collections=[self.summary_collection])
                    pre_dense = tf.layers.dense(inputs=pre_dense, units=n_hidden, activation=tf.nn.relu)

                input_print = tf.reshape(pre_dense, [batch_size, -1, self.n_hidden_pre_dense[-1]])

            # now we are ready to unroll the graph
            with tf.variable_scope('shallow'):
                outputs_shallow, final_state_shallow = tf.nn.dynamic_rnn(cell=cell_shallow,
                                                                         initial_state=self.initial_state[0],
                                                                         inputs=input_print,
                                                                         sequence_length=self.seq_lengths)

            with tf.variable_scope('medium'):
                outputs_medium, final_state_medium = tf.nn.dynamic_rnn(cell=cell_medium,
                                                                       initial_state=self.initial_state[1],
                                                                       inputs=tf.concat([input_print, outputs_shallow],
                                                                                        -1),
                                                                       sequence_length=self.seq_lengths)

            with tf.variable_scope('deep'):
                outputs_deep, final_state_deep = tf.nn.dynamic_rnn(cell=cell_deep,
                                                                   initial_state=self.initial_state[2],
                                                                   inputs=tf.concat([outputs_medium, outputs_shallow],
                                                                                    -1),
                                                                   sequence_length=self.seq_lengths)

            outputs = tf.concat([outputs_shallow, outputs_medium, outputs_deep], -1)

            final_state = tuple([final_state_shallow, final_state_medium, final_state_deep])

            # ----- DENSE LAYER IF CONFIGURED ----- #

            # The `outputs` tensor has shape `[batch_size, max_seq_length, hidden_size]`,
            # i.e. it contains the outputs of the last cell for every time step.
            # We want to map the output back to the "vocabulary space", so we add a dense layer.
            # Importantly, the dense layer should share its parameters across time steps.
            # To do this, we first flatten the outputs to `[batch_size*seq_length, hidden_size]`
            # and then add the dense layer.

            # debugging
            if not self.mode == 'inference' and self.debugging:
                outputs_print = tf.Print(outputs, [outputs], 'outputs\n')
            else:
                outputs_print = outputs

            outputs_flat = tf.reshape(outputs_print, [-1, self.n_hidden_rnn_shallow[-1] + self.n_hidden_rnn_medium[-1] +
                                                      self.n_hidden_rnn_deep[-1]])

            dense = tf.layers.dropout(inputs=outputs_flat, rate=self.dropout_rate_dense_pl, training=self.is_training)
            # create 'num_layers_encoder' dense layers
            for id_, n_hidden in enumerate(self.n_hidden_dense):
                tf.summary.histogram('post dense {}'.format(id_), dense, collections=[self.summary_collection])
                dense = tf.layers.dense(inputs=dense, units=n_hidden,
                                        activation=tf.nn.relu)
            # final dense layer has self.input_dim units
            dense_final = tf.layers.dense(inputs=dense, units=self.output_dim)

            # reshape back
            poses = tf.reshape(dense_final, [batch_size, self.max_seq_length, self.output_dim])

            tf.summary.histogram('dense final', poses, collections=[self.summary_collection])

            # debugging
            if not self.mode == 'inference' and self.debugging:
                poses_print = tf.Print(poses, [poses[0, 0, 0], poses[0, 1, 0], poses[0, 2, 0],
                                               poses[0, 0, 40], poses[0, 1, 40], poses[0, 2, 40],
                                               tf.shape(poses)], 'pose (and shape)\n')
            else:
                poses_print = poses

            # with tf.variable_scope('rnn_model', reuse=self.reuse):
            self.final_state = final_state
            self.prediction = poses_print

        # debugging output
        if self.debugging:
            print('\ninputs shape: {}'.format(self.input_.get_shape()))
            print('outputs shape: {}'.format(outputs.get_shape()))
            print('outputs flat shape: {}'.format(outputs_flat.get_shape()))
            print('poses flat shape: {}'.format(dense_final.get_shape()))
            print('mask shape: {} '.format(self.mask.get_shape()))
            print('poses shape: {}'.format(poses.get_shape()))
            print('target shape: {} \n'.format(self.target.get_shape()))

    def build_loss(self):
        """
        Builds the loss function.
        """
        # only need loss if we are not in inference mode
        if self.mode is not 'inference':
            with tf.name_scope('loss'):
                # TODO Implement your loss here
                # You can access the outputs of the model via `self.prediction` and the corresponding targets via
                # `self.target`. Hint 1: you will want to use the provided `self.mask` to make sure that padded values
                # do not influence the loss. Hint 2: L2 loss is probably a good starting point ...

                # debugging
                if not self.mode == 'inference' and self.debugging:
                    target_print = tf.Print(self.target,
                                            [self.target[0, 0, 0], self.target[0, 1, 0], self.target[0, 2, 0],
                                             self.target[0, 0, 40], self.target[0, 1, 40], self.target[0, 2, 40],
                                             tf.shape(self.target)], 'target (and shape)\n')
                else:
                    target_print = self.target

                # L2-loss
                mask_extended = tf.expand_dims(self.mask, axis=-1)
                self.loss = tf.losses.mean_squared_error(labels=target_print, predictions=self.prediction,
                                                         weights=mask_extended)

                tf.summary.scalar('loss', self.loss, collections=[self.summary_collection])

    def count_parameters(self):
        """
        Counts the number of trainable parameters in this model
        """
        self.n_parameters = 0
        for v in tf.trainable_variables():
            params = 1
            for s in v.get_shape():
                params *= s.value
            self.n_parameters += params

    def get_feed_dict(self, batch, dropout_rate_rnn=0, dropout_rate_dense=0):
        """
        Returns the feed dictionary required to run one training step with the model.
        :param batch: The mini batch of data to feed into the model
        :param dropout_rate_rnn: dropout rate for the dropout layer in lstm network (if given)
        :param dropout_rate_dense: dropout rate for the dropout layer in dense network (if given)
        :return: A feed dict that can be passed to a session.run call
        """
        input_padded, target_padded = batch.get_padded_data()

        feed_dict = {self.input_: input_padded,
                     self.target: target_padded,
                     self.seq_lengths: batch.seq_lengths,
                     self.mask: batch.mask,
                     self.dropout_rate_rnn_pl: dropout_rate_rnn,
                     self.dropout_rate_dense_pl: dropout_rate_dense}

        return feed_dict

    def build_lstm_cell_old(self, n_hidden_structure, name):
        """
        Returns a multi lstm cell with given structure
        :param n_hidden_structure: (list of number of hidden states per layer)
        :return: cell: rnn.MultiRNNCell structure
        """
        with tf.variable_scope(name):
            if self.is_training and self.dropout_rate_rnn > 0:
                cells = [
                    tf.contrib.rnn.DropoutWrapper(
                        tf.nn.rnn_cell.LSTMCell(num_units=n_hidden, reuse=self.reuse, forget_bias=1.0),
                        output_keep_prob=1 - self.dropout_rate_rnn_pl)
                    for id_, n_hidden in enumerate(n_hidden_structure)]
            elif self.mode == 'inference':
                cells = [tf.nn.rnn_cell.LSTMCell(num_units=n_hidden, reuse=self.reuse, forget_bias=1.0)
                         for id_, n_hidden in enumerate(n_hidden_structure)]
            else:
                cells = [tf.nn.rnn_cell.LSTMCell(num_units=n_hidden, reuse=self.reuse, forget_bias=1.0)
                         for id_, n_hidden in enumerate(n_hidden_structure)]

            # we stack the cells together and create one big RNN cell
            cell = tf.contrib.rnn.MultiRNNCell(cells)

        return cell

    def build_lstm_cell(self, n_hidden_structure, name):
        """
        Returns a multi lstm cell with given structure
        :param n_hidden_structure: (list of number of hidden states per layer)
        :return: cell: rnn.MultiRNNCell structure
        """
        with tf.variable_scope(name):
            cells = []

            for id_, n_hidden in enumerate(n_hidden_structure):
                if self.is_training and self.dropout_rate_rnn > 0 and id_ == 0:
                    cells.append(tf.contrib.rnn.DropoutWrapper(
                        tf.nn.rnn_cell.LSTMCell(num_units=n_hidden, reuse=self.reuse, forget_bias=1.0),
                        output_keep_prob=1 - self.dropout_rate_rnn_pl))

                elif self.mode == 'inference':
                    cells.append(tf.nn.rnn_cell.LSTMCell(num_units=n_hidden, reuse=self.reuse, forget_bias=1.0))

                else:
                    cells.append(tf.nn.rnn_cell.LSTMCell(num_units=n_hidden, reuse=self.reuse, forget_bias=1.0))

            # we stack the cells together and create one big RNN cell
            cell = tf.contrib.rnn.MultiRNNCell(cells)

        return cell
