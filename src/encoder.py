import tensorflow as tf


class EncoderModel(object):

    def __init__(self, config, placeholders, mode):

        assert mode in ['training', 'validation', 'inference']
        self.config = config
        self.input_ = placeholders['input_pl']
        self.target = placeholders['target_pl']
        self.mask = placeholders['mask_pl']
        self.action_mask = placeholders['action_mask_pl']
        self.seq_lengths = placeholders['seq_lengths_pl']
        self.mode = mode
        self.is_training = self.mode == 'training'
        self.reuse = self.mode == 'validation'
        self.onehot = config['onehot_action_labels_encoder']
        self.batch_size = tf.shape(self.input_)[0]  # dynamic size
        self.max_seq_length = tf.shape(self.input_)[1]  # dynamic size
        self.input_dim = config['input_dim_encoder']
        self.output_dim = config['output_dim']
        self.summary_collection = 'training_summaries' if mode == 'training' else 'validation_summaries'

        # newly added variables
        self.n_hidden = config['n_hidden_encoder']
        self.activation = config['activation']
        self.dropout_rate = config['dropout_rate_encoder']
        self.dropout_rate_pl = placeholders['dropout_rate_pl']
        self.l2_regularization = config['l2_regularization']

        self.debugging = config['debugging']


    def build_graph(self):
        self.build_model()
        self.build_loss()
        self.count_parameters()

    def build_model(self):

        with tf.variable_scope('encoder', reuse=self.reuse):

            scale = self.l2_regularization
            regularizer = tf.contrib.layers.l2_regularizer(scale=scale)

            #self.current_dropout_rate = tf.Variable(self.dropout_rate_pl, trainable=False)
            tf.summary.scalar('dropout_rate', self.dropout_rate_pl, collections=[self.summary_collection])

            # debugging
            if not self.mode == 'inference' and self.debugging:
                input_print = tf.Print(self.input_, [self.input_, tf.shape(self.input_)], 'input (and shape)\n')
            else:
                input_print = self.input_

            # reshape input from (batch_size, self.max_seq_length, input_dim) to (batch_size*self.max_seq_length, input_dim)
            # this is because we treat every "time step" as a sample. This increases the batch size by self.max_seq_length
            flat = tf.reshape(input_print, [-1, self.input_dim])

            # Add gaussian noise if we are in training mode
            if self.mode == 'training':
                noise = tf.random_normal([self.batch_size,1],mean=0.0,stddev=self.config['feature_stds'][0],dtype=tf.float32)
                for idx, std in enumerate(self.config['feature_stds']):
                    if idx > 0:
                        next_noise = tf.random_normal([self.batch_size,1],mean=0.0,stddev=std,dtype=tf.float32,)
                        noise = tf.concat([noise,next_noise],1)
                if self.onehot:
                    noise = tf.concat([noise,tf.zeros([self.batch_size,15])],1)
            else:
                noise = tf.zeros([self.batch_size, self.input_dim])
            
            input_noise = tf.add(flat, noise)
            # dropout
            dropout = tf.layers.dropout(inputs=input_noise, rate=self.dropout_rate, training=self.is_training)

            #dense = dropout
            dense = dropout

            # create 'num_layers_encoder' dense layers
            for n_hidden in self.n_hidden:
                
                dense = tf.layers.dense(inputs=dense, units=n_hidden,
                                        activation=tf.nn.relu, bias_regularizer=regularizer, kernel_regularizer=regularizer)

            # final dense layer has self.input_dim units
            #dense_final = tf.layers.dense(inputs=dense, units=self.input_dim)
            dense_final = tf.layers.dense(inputs=dense, units=self.output_dim, bias_regularizer=regularizer, kernel_regularizer=regularizer)

            # reshape obtained outputs back to (batch_size, self.max_seq_length, self.input_dim)
            batch_size = tf.shape(self.input_)[0]
            #outputs = tf.reshape(dense_final, [batch_size, self.max_seq_length, self.input_dim])
            outputs = tf.reshape(dense_final, [batch_size, self.max_seq_length, self.output_dim])

            # debugging
            if not self.mode == 'inference' and self.debugging:
                outputs_print = tf.Print(outputs, [outputs, tf.shape(outputs)], 'outputs\n')
            else:
                outputs_print = outputs

            self.prediction = outputs_print


    def build_loss(self):
        """
        Builds the loss function.
        """
        # only need loss if we are not in inference mode
        if self.mode is not 'inference':
            with tf.name_scope('loss'):
                # TODO Implement your loss here
                # You can access the outputs of the model via `self.prediction` and the corresponding targets via
                # `self.target`. Hint 1: you will want to use the provided `self.mask` to make sure that padded values
                # do not influence the loss. Hint 2: L2 loss is probably a good starting point ...

                # L2-loss
                mask_extended = tf.expand_dims(self.mask, axis=-1)
                action_mask_extended = tf.expand_dims(self.action_mask, axis=-1)
                mask_ = tf.multiply(mask_extended, action_mask_extended)
                self.loss = tf.losses.mean_squared_error(labels=self.target, predictions=self.prediction, weights=mask_)

                reg_term = tf.losses.get_regularization_loss()
                self.loss_reg = self.loss + reg_term
                self.loss = self.loss_reg

                tf.summary.scalar('loss', self.loss, collections=[self.summary_collection])

    def count_parameters(self):
        """
        Counts the number of trainable parameters in this model
        """
        self.n_parameters = 0
        for v in tf.trainable_variables():
            params = 1
            for s in v.get_shape():
                params *= s.value
            self.n_parameters += params

    def get_feed_dict(self, batch, dropout_rate=0, onehot=False):
        """
        Returns the feed dictionary required to run one training step with the model.
        :param batch: The mini batch of data to feed into the model
        :param dropout_rate: dropout rate for the dropout layer in dense network (if given)
        :return: A feed dict that can be passed to a session.run call
        """
        if onehot:
            input_padded, target_padded = batch.get_padded_data_with_onehot()
        else:
            input_padded, target_padded = batch.get_padded_data()
            
        action_mask = batch.action_mask()

        feed_dict = {self.input_: input_padded,
                     self.target: target_padded,
                     self.seq_lengths: batch.seq_lengths,
                     self.mask: batch.mask,
                     self.action_mask: action_mask,
                     self.dropout_rate_pl: dropout_rate}

        return feed_dict
