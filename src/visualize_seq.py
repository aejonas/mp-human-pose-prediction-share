import os
import tensorflow as tf
import numpy as np

from config import visualize_config
from visualize import visualize_joint_angles, visualize_positions, visualize_array
from utils import preprocessing_pipeline, forward_kinematics
from train import load_data, get_model_and_placeholders, get_encoder_model_and_placeholders


def main(config):
    # load the data
    data = load_data(config, 'train')

    # preprocessing of data
    if config['preprocessing']:
        preprocessing_pipeline_ = preprocessing_pipeline(config)

        # fit the transformations to the training input data
        input_training = np.concatenate(list(data.input_), axis=0)
        print('input training shape', input_training.shape)
        preprocessing_pipeline_.fit(input_training)

        # transform input and target for training and validation data with the fitted transformations
        for idx, _ in enumerate(data.input_):
            data.input_[idx] = preprocessing_pipeline_.transform(data.input_[idx])

    data_action = []
    firsts = [-5 for _ in range(15)]
    for action in range(15):
        for idx, _ in enumerate(data.input_):
            if data.action_labels[idx] == action and data.input_[idx].shape[0] == config['max_seq_length']:
                if firsts[action] < 1:
                    firsts[action] = firsts[action] + 1
                else:
                    angle_length = data.input_[idx].shape[0]
                    onehot = np.repeat(np.asmatrix(np.array([int(j == action) for j in range(15)], dtype='float64')),
                                       angle_length, 0)
                    data_action.append(np.append(data.input_[idx], onehot, 1))
                    break

    # pad batch to batch size
    for i in range(config['batch_size'] - 15):
        data_action.append(np.copy(data_action[0]))

    # strip 'prediction_length' number of images from the end of the sequence (we want to predict these)
    data_visualize = [
        np.array(np.delete(
            el,
            [el.shape[0] - x - 1 for x in range(config['prediction_length'])],
            0
        )) for el in data_action
    ]

    if config['onehot_action_labels']:
        config['input_dim_lstm'] = data.input_[0].shape[-1] + 15
    else:
        config['input_dim_lstm'] = data.input_[0].shape[-1]

    config['output_dim'] = data.input_[0].shape[-1]

    if config['onehot_action_labels_encoder']:
        config['input_dim_encoder'] = data.input_[0].shape[-1] + 15
    else:
        config['input_dim_encoder'] = data.input_[0].shape[-1]

    config['output_dim'] = data.input_[0].shape[-1]

    rnn_model, placeholders = get_model_and_placeholders(config)

    # restore the model by first creating the computational graph
    with tf.name_scope('inference'):
        rnn_model = rnn_model(config, placeholders, mode='inference')
        rnn_model.build_graph()

    if config['lstm_encoder']:
        encoder_model, placeholders_encoder = get_encoder_model_and_placeholders(config)
        with tf.name_scope('inference_encoder'):
            encoder_model = encoder_model(config, placeholders_encoder, mode='inference')
            encoder_model.build_graph()

    print('variable names of all trainable variables:\n')
    for var in tf.trainable_variables():
        print(var.name)

    # make sure the Savor only tries to load the variable corresponding to it's trained model (in checkpoint)
    all_vars = tf.trainable_variables()
    lstm_vars = [k for k in all_vars if k.name.startswith('lstm')]
    dense_vars = [k for k in all_vars if k.name.startswith('encoder')]

    with tf.Session() as sess:
        # now restore the trained variables
        # this operation will fail if this `config` does not match the config you used during training
        saver = tf.train.Saver(lstm_vars)
        ckpt_id = config['checkpoint_id']
        if ckpt_id is None:
            ckpt_path = tf.train.latest_checkpoint(config['model_dir'])
        else:
            ckpt_path = os.path.join(os.path.abspath(config['model_dir']), 'model-{}'.format(ckpt_id))
        print('Evaluating ' + ckpt_path)
        saver.restore(sess, ckpt_path)
        print('is this working????')

        if config['lstm_encoder']:
            # now restore the trained variables of the encoder (if confit['encoder'] == True)
            # this operation will fail if this `config` does not match the config you used during training
            saver_encoder = tf.train.Saver(dense_vars)
            ckpt_id = config['checkpoint_id_encoder']
            if ckpt_id is None:
                ckpt_path = tf.train.latest_checkpoint(config['model_dir_encoder'])
            else:
                ckpt_path = os.path.join(os.path.abspath(config['model_dir_encoder']), 'model-{}'.format(ckpt_id))
            print('combined with ' + ckpt_path)
            saver_encoder.restore(sess, ckpt_path)

        # loop through all the test samples
        seeds = []
        predictions = []
        ids = []

        # input_ = np.stack(data_visualize,axis=0)
        seq_lengths_ = [data_visualize[0].shape[0] for _ in range(len(data_visualize))]
        # initialize the RNN with the known sequence (here 2 seconds)
        # no need to pad the batch because in the test set all batches have the same length
        seeds.append(data_visualize)

        # here we are requesting the final state as we later want to supply this back into the RNN
        # this is why the model should have a member `self.final_state`

        input_onehot = np.array(data_visualize)
        if config['onehot_action_labels']:
            input_rnn = np.array(data_visualize)
        else:
            input_rnn = np.array([x[:, 0:x.shape[1] - 15] for x in data_visualize])

        fetch = [rnn_model.final_state]
        feed_dict = {placeholders['input_pl']: input_rnn,
                     placeholders['seq_lengths_pl']: seq_lengths_}

        [state] = sess.run(fetch, feed_dict)

        # debugging
        # next_pose_print = tf.Print(input_, [tf.shape(input_), input_[:, -1:]], 'next pose\n')

        # now get the prediction by predicting one pose at a time and feeding this pose back into the model to
        # get the prediction for the subsequent time step

        next_pose = input_rnn[:, -1:]
        onehot_arr = input_onehot[:, -1:, -15:]

        predicted_poses = []
        for f in range(config['prediction_length']):
            print('predicting frame nr ', f)

            fetch = [rnn_model.final_state, rnn_model.prediction]

            # debugging
            # next_pose_print_2 = tf.Print(next_pose, [tf.shape(next_pose)], 'next pose own assignment \n')
            # print('next pose 2nd: {}'.format(next_pose.shape))

            feed_dict = {placeholders['input_pl']: next_pose,
                         placeholders['seq_lengths_pl']: [1 for _ in range(config['batch_size'])],
                         rnn_model.initial_state: state}
            [state, predicted_pose] = sess.run(fetch, feed_dict)

            if config['lstm_encoder']:

                if config['onehot_action_labels_encoder']:
                    predicted_pose = np.append(predicted_pose, onehot_arr, 2)

                fetch_encoder = [encoder_model.prediction]
                # print('next_pose encoder shape: {}'.format(predicted_pose.shape))
                feed_dict_encoder = {placeholders_encoder['input_pl']: predicted_pose,
                                     placeholders['seq_lengths_pl']: [1 for _ in range(config['batch_size'])]}
                [predicted_pose] = sess.run(fetch_encoder, feed_dict_encoder)

            if config['onehot_action_labels']:
                predicted_poses.append(next_pose[:, :, 0:next_pose.shape[2] - 15])
                next_pose = np.append(predicted_pose, onehot_arr, 2)
            else:
                predicted_poses.append(np.copy(next_pose))
                next_pose = predicted_pose

        predicted_poses = np.concatenate(predicted_poses, axis=1)

        predictions.append(predicted_poses)

        seeds = np.concatenate(seeds, axis=0)
        predictions = np.concatenate(predictions, axis=0)

    # inverse transform predictions and input
    # transform input and target for training and validation data with the fitted transformations    

    if config['preprocessing']:
        for idx, _ in enumerate(data_action):
            data_action[idx] = preprocessing_pipeline_.inv_transform(data_action[idx])

        n_features = predictions.shape[-1]

        print('predictions and seeds before reshaping: ', predictions.shape, seeds.shape)
        predictions = predictions.reshape(-1, n_features)
        # seeds = seeds.reshape(-1, n_features)

        print('predictions and seeds after reshaping: ', predictions.shape, seeds.shape)
        predictions = preprocessing_pipeline_.inv_transform(predictions)
        # seeds = preprocessing_pipeline_.inv_transform(seeds)

        print('predictions and seeds after preprocessing: ', predictions.shape, seeds.shape)
        n_features = predictions.shape[-1]
        predictions = predictions.reshape(-1, visualize_config['prediction_length'], n_features)
        # seeds = seeds.reshape(-1, train_config['max_seq_length'], n_features)

        print('predictions and seeds after preprocessing and reshaping: ', predictions.shape, seeds.shape)
        print(predictions[0].shape)

    folder = '../images/'
    for i in range(15):
        print(predictions[i].shape)
        positions = forward_kinematics(predictions[i])
        visualize_array(positions, folder + str(i) + '_pred.png')

        orig = data_action[i]
        print(data_action[i].shape)
        if config['preprocessing_pipeline'] == 'remove_zeros':
            orig = np.delete(orig, [x + 51 for x in range(15)], 1)
        else:
            orig = np.delete(orig, [x + 75 for x in range(15)], 1)

        del_length = orig.shape[0] - visualize_config['prediction_length']
        orig = np.asarray(np.delete(orig, [x for x in range(del_length - 1)], 0))

        visualize_array(forward_kinematics(orig), folder + str(i) + '_orig.png')
