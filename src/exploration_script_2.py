from train import *
from config import train_config

print('started exploration\n')

learning_rates = [0.004]
decay_rates = [0.75]
thresholds = [1e1]
n_hidden_rnns = [[400], [600]]
n_hidden_denses = [[]]
dropout_rnns = [0.5]
#dropout_denses

train_config['encoder'] = False
train_config['preprocessing_pipeline'] = ['identity']  #pr-id, pr-std, pr-zor
train_config['preprocessing'] = False
train_config['n_epochs'] = 75
train_config['onehot_action_labels'] = True # oh
train_config['final_training'] = False
train_config['use_reverse'] = False
train_config['learning_rate_type'] = 'adaptive'
train_config['schedule'] = [1, 3]
train_config['warm_start'] = False

name_partial = 'lstm'
if train_config['onehot_action_labels']:
    name_partial += '_oh'

if train_config['use_reverse']:
    name_partial += '_re'

if train_config['preprocessing_pipeline'] == 'standard':
    name_partial += '_pr-std'
elif train_config['preprocessing_pipeline'] == 'zero-one_range':
    name_partial += '_pr-zor'

if train_config['warm_start']:
    name_partial += '_ws'


# iterate over all configurations and train the model
for learning_rate in learning_rates:
    for decay_rate in decay_rates:
        for threshold in thresholds:
            for n_hidden_rnn in n_hidden_rnns:
                for n_hidden_dense in n_hidden_denses:
                    for dropout_rnn in dropout_rnns:
                        dropout_dense = dropout_rnn
                        #for dropout_dense in dropout_denses:


                        # change learning rate
                        train_config['learning_rate'] = learning_rate
                        # change learning_rate_decay_rate
                        train_config['learning_rate_decay_rate'] = decay_rate
                        # change flattening_threshold
                        train_config['flattening_threshold'] = threshold
                        # change n_hidden_rnn
                        train_config['n_hidden_rnn'] = n_hidden_rnn
                        # change n_hidden_dense
                        train_config['n_hidden_dense'] = n_hidden_dense
                        # change dropout_rate_rnn
                        train_config['dropout_rate_rnn'] = dropout_rnn
                        # change dropout_rate_dense
                        train_config['dropout_rate_dense'] = dropout_dense

                        n_hidden_rnn_name = '-'.join(['%-2s' % (i,) for i in n_hidden_rnn])
                        n_hidden_dense_name = '-'.join(['%-2s' % (i,) for i in n_hidden_dense]) + '-75'

                        # change name (format: lstm_n-hidden-rnn_n-hidden-dense_dropouts_learning-rate-decay-rate_flattening-threshold)
                        name = '{}_{}_{}_{}-{}_{}_{}'.format(name_partial, n_hidden_rnn_name, n_hidden_dense_name, dropout_rnn, dropout_dense, decay_rate, threshold)
                        train_config['name'] = name

                        print('\nstart training:\n')
                        print(name, '\n')

                        if __name__ == '__main__':
                            main(train_config)





