import tensorflow as tf


class AutoEncoder(object):

    def __init__(self, config, placeholders, mode):

        assert mode in ['training', 'validation', 'inference']
        self.config = config
        self.input_ = placeholders['input_pl']
        self.target = placeholders['target_pl']
        self.mask = placeholders['mask_pl']
        self.seq_lengths = placeholders['seq_lengths_pl']
        self.mode = mode
        self.is_training = self.mode == 'training'
        self.reuse = self.mode == 'validation'
        self.batch_size = tf.shape(self.input_)[0]  # dynamic size
        self.max_seq_length = tf.shape(self.input_)[1]  # dynamic size
        self.input_dim = config['input_dim']
        self.output_dim = config['output_dim']
        self.summary_collection = 'training_summaries' if mode == 'training' else 'validation_summaries'

        self.dropout_rate = config['dropout_rate_encoder']
        self.n_hidden_encoder = config['n_hidden_encoder']

    def build_graph(self):
        self.build_model()
        self.build_loss()
        self.count_parameters()

    def build_model(self):

        # TODO Implement your model here
        # Some hints:
        #   1) You can access an input batch via `self.input_` and the corresponding targets via `self.target`. Note
        #      that the shape of each input and target is (batch_size, max_seq_length, input_dim)
        #
        #   2) The sequence length of each batch entry is variable, i.e. one entry in the batch might have length
        #      99 while another has length 67. No entry will be larger than what you supplied in
        #      `self.config['max_seq_length']`. This maximum sequence length is also available via `self.max_seq_length`
        #      Because TensorFlow cannot handle variable length sequences out-of-the-box, the data loader pads all
        #      batch entries with zeros so that they have size `self.max_seq_length`. The true sequence lengths are
        #      stored in `self.seq_lengths`. Furthermore, `self.mask` is a mask of shape
        #      `(batch_size, self.max_seq_length)` whose entries are 0 if this entry was padded and 1 otherwise.
        #
        #   3) You can access the config via `self.config`
        #
        #   4) The following member variables should be set after you complete this part:
        #      - `self.initial_state`: a reference to the initial state of the RNN
        #      - `self.final_state`: the final state of the RNN after the outputs have been obtained
        #      - `self.prediction`: the actual output of the model in shape `(batch_size, self.max_seq_length, output_dim)`

        # reshape input from (batch_size, self.max_seq_length, input_dim) to (batch_size*self.max_seq_length, input_dim)
        # this is because we treat every "time step" as a sample. This increases the batch size by self.max_seq_length
        flat = tf.reshape(self.input_, [-1, self.input_dim])

        # dropout
        dropout = tf.layers.dropout(inputs=flat, rate=self.dropout_rate, training=self.is_training)

        dense = dropout

        # create 'num_layers_encoder' dense layers
        for n_hidden in self.n_hidden_encoder:
            dense = tf.layers.dense(inputs=dense, units=n_hidden,
                                    activation=tf.nn.relu)

        # final dense layer has self.input_dim units
        densefinal = tf.layers.dense(inputs=dense, units=self.input_dim)

        # reshape obtained outputs back to (batch_size, self.max_seq_length, self.input_dim)
        batch_size = tf.shape(self.input_)[0]
        outputs = tf.reshape(densefinal, [batch_size, self.max_seq_length, self.input_dim])

        with tf.variable_scope('ae_model', reuse=self.reuse):
            # poses_print = tf.Print(poses, [poses], 'pose values of prediction:')
            # self.prediction = poses
            self.prediction = outputs

    def build_loss(self):
        """
        Builds the loss function.
        """
        # only need loss if we are not in inference mode
        if self.mode is not 'inference':
            with tf.name_scope('loss'):
                # TODO Implement your loss here
                # You can access the outputs of the model via `self.prediction` and the corresponding targets via
                # `self.target`. Hint 1: you will want to use the provided `self.mask` to make sure that padded values
                # do not influence the loss. Hint 2: L2 loss is probably a good starting point ...

                target_masked = tf.boolean_mask(self.input_, self.mask)
                prediction_masked = tf.boolean_mask(self.prediction, self.mask)

                self.loss = tf.losses.mean_squared_error(labels=target_masked, predictions=prediction_masked)
                tf.summary.scalar('loss', self.loss, collections=[self.summary_collection])

    def count_parameters(self):
        """
        Counts the number of trainable parameters in this model
        """
        self.n_parameters = 0
        for v in tf.trainable_variables():
            params = 1
            for s in v.get_shape():
                params *= s.value
            self.n_parameters += params

    def get_feed_dict(self, batch):
        """
        Returns the feed dictionary required to run one training step with the model.
        :param batch: The mini batch of data to feed into the model
        :return: A feed dict that can be passed to a session.run call
        """
        input_padded, target_padded = batch.get_padded_data()

        feed_dict = {self.input_: input_padded,
                     self.target: target_padded,
                     self.seq_lengths: batch.seq_lengths,
                     self.mask: batch.mask}

        return feed_dict
