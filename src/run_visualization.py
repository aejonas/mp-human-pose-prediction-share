import tensorflow as tf
from evaluate_test import *
from evaluate_test import main as eval
from config import test_config, train_config

# add command line arguments to config
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-model_path', type=str, required=False,
                    help='path of directory containing trained model e.g. /path/of/directory/lstm_1554648920.py')

args = parser.parse_args()
model_path = args.model_path

tf.reset_default_graph()

test_config['model_dir'] = '/Users/aejonas/mp-human-prediction-example-code/trained_models/lstm_1547144672_2/'
eval(test_config)
# '/Users/aejonas/mp-human-prediction-example-code/trained_models/lstm_1546980771_2'